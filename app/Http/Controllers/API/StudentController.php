<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Resources\StudentCollection;
use App\Http\Resources\StudentResource;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentController extends Controller
{
    protected $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }
    /**
     * Display a listing of the resource.
     */

    public function index()
    {
        $students = $this->student->paginate(5);

        $studentCollection = new StudentCollection($students);

        return $this->sentSuccessResponse($studentCollection, 'Success', Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStudentRequest $request)
    {
        $dataCreate = $request->all();

        $student = $this->student->create($dataCreate);

        $studentResource = new StudentResource($student);

        return $this->sentSuccessResponse($studentResource, 'Success', Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $student = $this->student->findOrFail($id);

        $studentResource = new StudentResource($student);

        return $this->sentSuccessResponse($studentResource,'Success', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStudentRequest $request, $id)
    {
        $updateData = $request->all();

        $student = $this->student->findOrFail($id);

        $student->update($updateData);

        $studentResource = new StudentResource($student);

        return $this->sentSuccessResponse($studentResource,'Success', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $student = $this->student->findOrFail($id);

        $student->delete();

        $studentResource = new StudentResource($student);

        return $this->sentSuccessResponse($studentResource,'Success', Response::HTTP_OK);


    }
}
