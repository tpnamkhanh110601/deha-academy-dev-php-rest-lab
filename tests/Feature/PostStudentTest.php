<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class PostStudentTest extends TestCase
{
   /** @test */

    public function user_can_create_student_if_data_is_valid()
    {
        $dataCreate = [
            'name' => $this->faker->name,
            'address' => $this->faker->address,
        ];

        $response = $this->json('POST', route('students.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('name', $dataCreate['name'])
                ->etc()
            )
            ->etc()
        );

        $this->assertDatabaseHas('students', [
            'name' => $dataCreate['name'],
            'address' => $dataCreate['address']
        ]);
    }

    /** @test */

    public function user_can_not_create_student_if_name_is_null()
    {
        $dataCreate = [
            'name' => '',
            'address' => $this->faker->address,
        ];

        $response = $this->postjson(route('students.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('name')
                ->etc()
            )
        );
    }

    /** @test  */

    public function user_can_not_create_student_if_address_is_null()
    {
        $dataCreate = [
            'name' => $this->faker->name,
            'body' => ''
        ];

        $response = $this->postJson(route('students.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->where('address',['Please enter students address'])
                ->etc()
            )
        );
    }

    /** @test */
    public function user_can_not_create_student_if_data_is_not_valid(){
        $dataCreate = [
            'name' => '',
            'address' => ''
        ];

        $responose = $this->postJson(route('students.store'),$dataCreate);

        $responose->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $responose->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->where('name', ['Please enter student name'])
                ->where('address', ['Please enter students address'])
                ->etc()
            )
        );
    }
}
