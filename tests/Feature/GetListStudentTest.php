<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListStudentTest extends TestCase
{
    /** @test */

    public function user_can_get_list_students()
    {
        $response = $this->getJson(route('students.index'));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->has('data', fn(AssertableJson $json) =>
                    $json->each(fn (AssertableJson $value) =>
                        $value->has('name')->etc()
                    )->etc()
                )
            )
            ->has('message')
            ->etc()
        );
    }
}
