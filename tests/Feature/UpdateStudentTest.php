<?php

namespace Tests\Feature;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use function Symfony\Component\Translation\t;

class UpdateStudentTest extends TestCase
{
    /** @test  */

    public function user_can_update_student_if_data_is_valid()
    {

        $student = Student::factory()->create();

        $dataUpdate = [
            'name' => $this->faker->name,
            'address' => $this->faker->address,
        ];

        $response = $this->putJson(route('students.update', $student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $dataUpdate['name'])
                ->where('address', $dataUpdate['address'])
            )
            ->etc()
        );

        $this->assertDatabaseHas('students', [
            'name' => $dataUpdate['name'],
            'address' => $dataUpdate['address']
        ]);
    }

    /** @test */

    public function user_can_not_update_student_if_name_is_null() {
        $student = Student::factory()->create();

        $dataUpdate = [
            'name' => '',
            'address' => $this->faker->address,
        ];

        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors',fn(AssertableJson $json) =>
                $json->where('name', ['Please enter student\'s name'])
                ->etc()
            )
        );
    }

    /** @test */

    public function user_can_not_update_student_if_address_is_null() {
        $student = Student::factory()->create();

        $dataUpdate = [
            'name' => $this->faker->name,
            'address' => ''
        ];

        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->where('address', ['Please enter student\'s address'])
            ->etc()
        )
        );
    }

    /** @test */
    public function user_can_not_update_student_if_data_not_valid() {
        $student = Student::factory()->create();

        $dataUpdate = [
            'name' => '',
            'address' => ''
        ];

        $response = $this->putJson(route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors',fn(AssertableJson $json) =>
        $json->where('address', ['Please enter student\'s address'])
        ->where('name', ['Please enter student\'s name'])
        ->etc()
        )
        );
    }
}

